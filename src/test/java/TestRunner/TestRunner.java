package TestRunner;

import org.junit.runner.RunWith;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;





@CucumberOptions(
		
		features = "src\\test\\java\\features" ,
		
		glue = "stepDefination" , 
		
		plugin = {"pretty" , "html:target/cucumber"} ,
		
		monochrome=true
			
		
		)


public class TestRunner extends AbstractTestNGCucumberTests{

	
	
}
