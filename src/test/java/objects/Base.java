package objects;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	
	
	protected static WebDriver driver;
	
	protected static Properties  prop;
	
	
	public static WebDriver start() throws IOException {
		
		FileInputStream fis = new FileInputStream("C:\\Users\\Prerna Pandey\\eclipse-workspace\\Mynt\\resources\\data.properties");
		
		prop=new Properties();
		
		prop.load(fis);
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Prerna Pandey\\Desktop\\chrome v-87\\chromedriver_win32 (1)\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(80000, TimeUnit.SECONDS);
	
		driver.get("https://www.myntra.com/?utm_source=Google&utm_medium=cpc&utm_campaign=Search%20-%20Myntra%20Brand%20(India)&gclid=Cj0KCQiAqo3-BRDoARIsAE5vnaKxl3nGk1Lrw2mwvXmJkJ6H8ghGSyHY5gNO7_NTBzee1I6oUUvAv8MaAvoyEALw_wcB");
		
		
		
		
		return driver;
	}

}
