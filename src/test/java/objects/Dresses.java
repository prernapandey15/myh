package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Dresses {
	
	
	WebDriver driver;
	
	By blackdress = By.xpath("//*[@id='desktopSearchResults']/div[2]/section/ul/li[1]/a/div[1]/div/div/div/picture/img");
	
	By amount = By.xpath("//*[@id='desktopSearchResults']/div[2]/section/ul/li[1]/a/div[2]/div/span[1]/span[1]");
	
	public Dresses(WebDriver driver) {
		
		this.driver=driver;
		
	}
	
	public void a1() {
		
		driver.findElement(blackdress).click();
		
	}
	
	public WebElement a2() {
		
		return driver.findElement(amount);
	}

}
