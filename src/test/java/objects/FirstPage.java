package objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class FirstPage {
	
	WebDriver driver;
	
	By women = By.xpath("//*[@id='desktop-header-cnt']/div[2]/nav/div/div[2]/div/a");
	By ethnicDresses = By.xpath("//a[text()='Ethnic Dresses']");
	
	Actions a;
	
	public FirstPage(WebDriver driver)
	{
		
	this.driver=driver;	
		
		
	}
	
	
	public void m1() {
		
		a=new Actions(driver);
		
		a.moveToElement(driver.findElement(women)).build().perform();
		
	}
	
	
	public void m2() {
		
		
		driver.findElement(ethnicDresses).click();
		
		System.out.println("dresses");
		
	}
}
