package stepDefination;

import org.testng.annotations.BeforeClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objects.Base;
import objects.Dresses;

public class ZstepDefination extends Base{
	
	

    @Given("^User is on Greencart Landing page$")
    public void user_is_on_greencart_landing_page() throws Throwable {
        //throw new PendingException();
    	
    	//System.out.println("a");
    }

    @When("^User searched for Cucumber Vegetable$")
    public void user_searched_for_cucumber_vegetable() throws Throwable {
        //throw new PendingException();
    	
    	//System.out.println("c");
    }

    @Then("^\"([^\"]*)\" results are displayed$")
    public void something_results_are_displayed(String strArg1) throws Throwable {
        //throw new PendingException();
    	
    	//System.out.println("d");
    }
    
    
    @Given("^Validate that user is landed on the login page$")
    public void validate_that_user_is_landed_on_the_login_page() throws Throwable {
        //throw new PendingException();
    	
    	//System.out.println("x");
    }

    @When("^Validate that user enters correct id and pass$")
    public void validate_that_user_enters_correct_id_and_pass() throws Throwable {
        //throw new PendingException();
    	
    	//System.out.println("y");
    }

    @Then("^Validate that success message is displayed$")
    public void validate_that_success_message_is_displayed() throws Throwable {
        //throw new PendingException();
    	
    //	System.out.println("z");
    }


    @Given("^Validate that user is landed on login page$")
    public void validate_that_user_is_landed_on_login_page() throws Throwable {
       // throw new PendingException();
    	
    //	System.out.println("p");
    }

    @When("^Validate that user enters incorrect id and pass$")
    public void validate_that_user_enters_incorrect_id_and_pass() throws Throwable {
       // throw new PendingException();
    	
    //	System.out.println("q");
    }

    @Then("^Validate that user lands on 403 error page$")
    public void validate_that_user_lands_on_403_error_page() throws Throwable {
       // throw new PendingException();
    	
    	//System.out.println("r");
    }

}
