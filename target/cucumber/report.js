$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("shoping.feature");
formatter.feature({
  "line": 1,
  "name": "Validate that user should be able to click on ADD TO CART",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Validate that user should be able to click on ADD TO CART for Ethnic dresses",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-should-be-able-to-click-on-add-to-cart-for-ethnic-dresses",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@SmokeTest1"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "Validate that user is landed on the page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "user click on \u003citem1\u003e And then clicks on \u003citem2\u003e and then user should be able to click on \u003citem3\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Validate that user is able to navigate to Proceed to payment page",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-should-be-able-to-click-on-add-to-cart-for-ethnic-dresses;",
  "rows": [
    {
      "cells": [
        "item1",
        "item2",
        "item3"
      ],
      "line": 14,
      "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-should-be-able-to-click-on-add-to-cart-for-ethnic-dresses;;1"
    },
    {
      "cells": [
        "Women",
        "Ethnic Dresses",
        "xxx"
      ],
      "line": 15,
      "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-should-be-able-to-click-on-add-to-cart-for-ethnic-dresses;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 15,
  "name": "Validate that user should be able to click on ADD TO CART for Ethnic dresses",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-should-be-able-to-click-on-add-to-cart-for-ethnic-dresses;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@SmokeTest1"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "Validate that user is landed on the page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "user click on Women And then clicks on Ethnic Dresses and then user should be able to click on xxx",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Validate that user is able to navigate to Proceed to payment page",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepDefinations.validate_that_user_is_landed_on_the_page()"
});
formatter.result({
  "duration": 7125472800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Women",
      "offset": 14
    },
    {
      "val": "Ethnic Dresses",
      "offset": 39
    },
    {
      "val": "xxx",
      "offset": 95
    }
  ],
  "location": "MyStepDefinations.user_click_on_and_then_clicks_on_and_then_user_should_be_able_to_click_on(String,String,String)"
});
formatter.result({
  "duration": 1940754700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepDefinations.validate_that_user_is_able_to_navigate_to_proceed_to_payment_page()"
});
formatter.result({
  "duration": 45500,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Search for items and validate results",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;search-for-items-and-validate-results",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@SmokeTest1"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "User is on Greencart Landing page",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "User searched for Cucumber Vegetable",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "\"Cucumber\" results are displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "ZstepDefination.user_is_on_greencart_landing_page()"
});
formatter.result({
  "duration": 98700,
  "status": "passed"
});
formatter.match({
  "location": "ZstepDefination.user_searched_for_cucumber_vegetable()"
});
formatter.result({
  "duration": 26200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cucumber",
      "offset": 1
    }
  ],
  "location": "ZstepDefination.something_results_are_displayed(String)"
});
formatter.result({
  "duration": 69200,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Validate that user is able to login",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-is-able-to-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@Reg1"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "Validate that user is landed on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Validate that user enters correct id and pass",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Validate that success message is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "ZstepDefination.validate_that_user_is_landed_on_the_login_page()"
});
formatter.result({
  "duration": 37000,
  "status": "passed"
});
formatter.match({
  "location": "ZstepDefination.validate_that_user_enters_correct_id_and_pass()"
});
formatter.result({
  "duration": 19300,
  "status": "passed"
});
formatter.match({
  "location": "ZstepDefination.validate_that_success_message_is_displayed()"
});
formatter.result({
  "duration": 18800,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Validate that user is not able to entere with incorrect credentials",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-is-not-able-to-entere-with-incorrect-credentials",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 32,
      "name": "@Testcase2"
    }
  ]
});
formatter.step({
  "line": 34,
  "name": "Validate that user is landed on login page",
  "keyword": "Given "
});
formatter.step({
  "line": 35,
  "name": "Validate that user enters incorrect id and pass",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "Validate that user lands on 403 error page",
  "keyword": "Then "
});
formatter.match({
  "location": "ZstepDefination.validate_that_user_is_landed_on_login_page()"
});
formatter.result({
  "duration": 83500,
  "status": "passed"
});
formatter.match({
  "location": "ZstepDefination.validate_that_user_enters_incorrect_id_and_pass()"
});
formatter.result({
  "duration": 28800,
  "status": "passed"
});
formatter.match({
  "location": "ZstepDefination.validate_that_user_lands_on_403_error_page()"
});
formatter.result({
  "duration": 38200,
  "status": "passed"
});
formatter.scenario({
  "line": 39,
  "name": "Vaildate that user should be able to see results",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;vaildate-that-user-should-be-able-to-see-results",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 38,
      "name": "@Testcase3"
    }
  ]
});
formatter.step({
  "line": 40,
  "name": "Validate that info is correct",
  "keyword": "Given "
});
formatter.step({
  "line": 41,
  "name": "Validate that data is populated",
  "rows": [
    {
      "cells": [
        "firstname",
        "lastname"
      ],
      "line": 42
    },
    {
      "cells": [
        "aaa",
        "vvv"
      ],
      "line": 43
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "Validate that correct message should be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepDefinations.validate_that_info_is_correct()"
});
formatter.result({
  "duration": 93100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepDefinations.validate_that_data_is_populated(DataTable)"
});
formatter.result({
  "duration": 2827000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepDefinations.validate_that_correct_message_should_be_displayed()"
});
formatter.result({
  "duration": 76400,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "Validate that user log",
  "description": "",
  "id": "validate-that-user-should-be-able-to-click-on-add-to-cart;validate-that-user-log",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 47,
  "name": "Validate that user is stupaid",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "Validate that user is master",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "Validate that user jkl",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepDefinations.validate_that_user_is_stupaid()"
});
formatter.result({
  "duration": 278700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepDefinations.validate_that_user_is_master()"
});
formatter.result({
  "duration": 219300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepDefinations.validate_that_user_jkl()"
});
formatter.result({
  "duration": 165600,
  "status": "passed"
});
});